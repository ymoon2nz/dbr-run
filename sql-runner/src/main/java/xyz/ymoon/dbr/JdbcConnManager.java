package xyz.ymoon.dbr;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Database Connection Manager for JDBC connections.
 * This program create different type of a vendor Database connection for JDBC base on url.
 * 
 * @author	Yoon Moon
 * @version	0.1
 *
 */
public class JdbcConnManager {

	public static String URL_DELIMITER = " ";
	public static int DEFAULT_ISOLATION = Connection.TRANSACTION_READ_UNCOMMITTED; 
	
	public static Connection getConnection(String url) {
		
		String[] urlArr = url.split(URL_DELIMITER);
		return getConnection(urlArr[0], urlArr[1], urlArr[2]);
	}
	
	public static Connection getConnection(String url, String user, String password) {
		Connection conn = null;
		// MS SQL server JDBC driver
		if (url.contains("jdbc:sqlserver:")) {
			try {
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
				conn = DriverManager.getConnection(url, user, password);
				conn.setTransactionIsolation(DEFAULT_ISOLATION);
			} catch (ClassNotFoundException e) {
				System.err.println("**** Could not load JDBC driver: jdbc:sqlserver");
				System.err.println("**** Exception: " + e);
				e.printStackTrace();
			} catch(SQLException ex) {
				System.err.println("**** SQLException information");
				while(ex!=null) {
					System.err.println ("**** Error msg: " + ex.getMessage());
					System.err.println ("**** SQLState: " + ex.getSQLState());
					System.err.println ("**** Error code: " + ex.getErrorCode());
					ex.printStackTrace();
					ex = ex.getNextException(); // For drivers that support chained exceptions
				}
			}

		// jdbc:db2://host:port/database
		} else if (url.contains("jdbc:db2:")) {	
			try {
				Class.forName("com.ibm.db2.jcc.DB2Driver");
				Properties prop = new Properties();
				prop.put("user", user);
				prop.put("password", password);
								
				conn = DriverManager.getConnection(url, prop);
				conn.setTransactionIsolation(DEFAULT_ISOLATION);
				//conn.setReadOnly(true);
			} catch (ClassNotFoundException e) {
				System.err.println("**** Could not load JDBC driver: jdbc:db2");
				System.err.println("**** Exception: " + e);
				e.printStackTrace();
			} catch(SQLException ex) {
				System.err.println("**** SQLException information");
				while(ex!=null) {
					System.err.println ("**** Error msg: " + ex.getMessage());
					System.err.println ("**** SQLState: " + ex.getSQLState());
					System.err.println ("**** Error code: " + ex.getErrorCode());
					ex.printStackTrace();
					ex = ex.getNextException(); // For drivers that support chained exceptions
				}
			}
		
		// jdbc:postgresql://host:port/database
		} else if (url.contains("jdbc:postgresql:")) {
			try {
				Class.forName("org.postgresql.Driver");
				conn = DriverManager.getConnection(url, user, password);
			} catch (ClassNotFoundException e) {
				System.err.println("**** Could not load JDBC driver: jdbc:postgresql");
				System.err.println("**** Exception: " + e);
				e.printStackTrace();
			} catch(SQLException ex) {
				System.err.println("**** SQLException information");
				while(ex!=null) {
					System.err.println ("**** Error msg: " + ex.getMessage());
					System.err.println ("**** SQLState: " + ex.getSQLState());
					System.err.println ("**** Error code: " + ex.getErrorCode());
					ex.printStackTrace();
					ex = ex.getNextException(); // For drivers that support chained exceptions
				}
			}
		// jdbc:mysql://host:port/database
		} else if (url.contains("jdbc:mysql:")) {
			try {
				Class.forName("com.mysql.cj.jdbc.Driver");
				conn = DriverManager.getConnection(url, user, password);
			} catch (ClassNotFoundException e) {
				System.err.println("**** Could not load JDBC driver: jdbc:mysql");
				System.err.println("**** Exception: " + e);
				e.printStackTrace();
			} catch(SQLException ex) {
				System.err.println("**** SQLException information");
				while(ex!=null) {
					System.err.println ("**** Error msg: " + ex.getMessage());
					System.err.println ("**** SQLState: " + ex.getSQLState());
					System.err.println ("**** Error code: " + ex.getErrorCode());
					ex.printStackTrace();
					ex = ex.getNextException(); // For drivers that support chained exceptions
				}
			}

/** 		} else if (url.contains("jdbc:oracle")) {			

			try {
				DriverManager.registerDriver (new oracle.jdbc.OracleDriver());
				//Class.forName("oracle.jdbc.driver.OracleDriver");
				Properties props = new Properties();
				props.setProperty("user", user);
				props.setProperty("password", password);
				props.setProperty(OracleConnection.CONNECTION_PROPERTY_THIN_NET_CONNECT_TIMEOUT, "12000");
				conn = DriverManager.getConnection(url, props);
				//conn.setTransactionIsolation(DEFAULT_ISOLATION);
				//conn.setReadOnly(true);
			} catch (SQLException e) {
				e.printStackTrace();
			} */

		} else {
			try {
				throw new ClassNotFoundException("Can't find class for url : "+url);
			} catch (Exception e) {e.printStackTrace();}
		}
		return conn;
	}
}
