package xyz.ymoon.dbr;

import java.sql.Connection; 
import java.sql.SQLException; 
import java.sql.Statement; 
import java.sql.ResultSet; 
import java.sql.ResultSetMetaData; 

import java.io.IOException; 
import java.io.FileReader;
import java.io.BufferedReader;

import java.util.Scanner;


public class JdbcApi {
    public Connection conn;

    public JdbcApi() {

    }

    public JdbcApi(String url, String user, String password) {
        conn = getConnection(url, user, password);
    }

    public Connection getConnection() {
        return conn;
    }

    public Connection getConnection(String url, String user, String password) {
        try {
            if (conn != null && !conn.isClosed()) {
                conn.close();
            } 
        } catch (SQLException e) { }
        conn = null;
        conn = JdbcConnManager.getConnection(url, user, password);
        return conn;
    }

    public boolean executeMultipleSQLsFile(String file) {
        String SQLs = new String();
        try {
            boolean isSuccess = true;
            FileReader fileReader = new FileReader(file);
            BufferedReader br = new BufferedReader(fileReader);
            String line;
            while((line = br.readLine()) != null && isSuccess) {
                SQLs += line +"\n";
            }
            br.close();
        } catch (IOException ie) {
            ie.printStackTrace(); 
        }
        return executeMultipleSQLs(SQLs);
    }

    public boolean executeMultipleSQLs(String SQLs) {
        /** Termination Settings
        --<ScriptOptions statementTerminator=";"/>
        --#SET TERMINATOR ;
         */

         //String sql = "SELECT TABNAME FROM SYSCAT.TABLES WHERE TABSCHEMA NOT LIKE 'SYS%'";
        String sql = "";
        String terminator = ";";

        try {
            conn.setAutoCommit(false);
            boolean isSuccess = true;

            String line;
            int lineNo = 1;
            int statementNo = 1;
            Scanner scanner = new Scanner(SQLs);

            while(scanner.hasNextLine() && isSuccess) {
                line = scanner.nextLine();
                if (line.contains("ScriptOptions statementTerminator=")) {
                    terminator = line.replaceAll("/","").replaceAll(" ","").replaceAll("\"","").replace("<", "").replace(">", "").split("=")[1];
                    System.err.println("**** terminator="+terminator);
                } else if(line.contains("#SET TERMINATOR")) {
                    terminator = line.replaceAll("-","").replace("#SET TERMINATOR", "").replace(" ", "");
                    System.err.println("**** terminator="+terminator);
                } else if (line.contains(terminator)){
                    //line.replace(terminator, "\n");
                    sql += line.split(terminator, 2)[0] ;
                    System.err.println("**** Statement("+statementNo+"): "+sql);
                    isSuccess = executeSQL(sql);
                    //sql = "";
                    sql = line.split(terminator, 2)[1] ;
                    statementNo += 1;
                } else {
                    sql += line +"\n";
                }
                //System.err.println(line);
                lineNo += 1;
            }
            scanner.close();
            if (isSuccess) {
                // Connection must be on a unit-of-work boundary to allow close
                conn.commit();
                System.err.println("**** Transaction committed" );
                System.err.println("**** Disconnected from data source");
                System.err.println("**** JDBC Exit from class RunSQL - no errors");
            } else {
                conn.rollback();
                System.err.println("**** Transaction rollback - Line ("+lineNo+")" );
                System.err.println("**** JDBC Exit from class RunSQL - error(s)");
            }
            
            // Close the connection
            conn.close();                                                            
            
        } catch(SQLException ex) {
            System.err.println("SQLException information");
            while(ex!=null) {
                System.err.println ("**** Error msg: " + ex.getMessage());
                System.err.println ("**** SQLState: " + ex.getSQLState());
                System.err.println ("**** Error code: " + ex.getErrorCode());
                ex.printStackTrace();
                ex = ex.getNextException(); // For drivers that support chained exceptions
            }
        }
        return true;
    }

    public boolean executeSQL(String sql) {
        Statement stmt;
        ResultSet rs;
        //conn = getConnection(url, user, password);
        if (conn == null) {
            return false;
        }
        try {                                                                        
            // Create the Statement
            stmt = conn.createStatement();                                           
            System.err.println("**** Created Statement object");

            // Execute a query and generate a ResultSet instance
            boolean isResultSet = stmt.execute(sql);
            if (isResultSet) {
                rs = stmt.getResultSet();
                ResultSetMetaData rsmd = rs.getMetaData();
                System.err.println("**** Created ResultSet object");
                int colNo = rsmd.getColumnCount();
                String row = "";
                for (int i=1; i<=colNo; i++){
                    if (!row.equals("")) {row += ",";}
                    row += rsmd.getColumnName(i);
                }
                System.out.println("ColumnNames: "+row);
                
                // Print all of the rows from ResultSet
                int rowNo = 1;
                while (rs.next()) {
                    row = "";
                    for (int i=1; i<=colNo; i++){
                        if (!row.equals("")) {row += ",";}
                        row += rs.getString(i);
                    }
                    System.out.println("Row ("+rowNo+"): "+row);
                    rowNo += 1;
                }
                System.err.println("**** Fetched all rows from ResultSet");
                // Close the ResultSet
                rs.close();
                System.err.println("**** Closed ResultSet");
            } else {
                System.err.println("**** No ResultSet");
            }
            // Close the Statement
            stmt.close();
            System.err.println("**** Closed Statement");

        } catch(SQLException ex) {
            System.err.println("**** SQLException information");
            while(ex!=null) {
                System.err.println ("**** Error msg: " + ex.getMessage());
                System.err.println ("**** SQLState: " + ex.getSQLState());
                System.err.println ("**** Error code: " + ex.getErrorCode());
                ex.printStackTrace();
                ex = ex.getNextException(); // For drivers that support chained exceptions
            }
            return false;
        }
        return true;
    }
}

