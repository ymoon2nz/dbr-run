package xyz.ymoon.dbrrun;

import xyz.ymoon.dbr.JdbcApi;

public class SQLRunner {
    public static void main(String[] args) {
        System.err.println("**** SQL-Runner Starting..."); 
        if (args[0]==null && args[1]==null && args[2]==null && args[3]==null) {
            System.err.println("**** Syntax: java -jar runner.-#.#.jar (action) (url) (user) (password) (SQL file)");
            System.exit(-1);
        }

        
        String url = args[0];
        String user = args[1];
        String password = args[2];
        String input = args[3];
        String action;
        try {
            action = input.split(":")[0];
                    
            JdbcApi ja = new JdbcApi(url, user, password);

            if (action.equals("file")) {
                ja.executeMultipleSQLsFile(input.split(":")[1]);
            } else if (action.equals("sql")) {
                ja.executeSQL(input.split(":")[2]);
            }
        } catch (Exception e) {
            java.util.Date date = new java.util.Date();
            System.err.println("**** "+date+" ERROR: SQL-Runner");
            System.err.println("**** Exception: " + e);
        }

    }
}
