--<ScriptOptions statementTerminator="@"/>
--#SET TERMINATOR @

BEGIN
    INSERT INTO TEST VALUES (1,'hello world');
    INSERT INTO TEST VALUES (2,'hello db2');
    select tabname from syscat.tables where tabschema not like 'SYS%' limit 20;
END@
