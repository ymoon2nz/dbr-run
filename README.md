dbRRun - Database Remote Run
-----------

## Applications

### sql-runner
Remote SQL Runner
*  Build
```
mvn package
```
* Syntax
```
java -jar target/sql-runner-spring-boot.jar {jdbc url} {username} {password} file:{path/file}|sql:{SQL}
```
* Run
```
cd sql-runner
java -jar target/sql-runner-spring-boot.jar jdbc:db2://localhost:50000/DOCKER db2inst1 admin file:/tmp/test-run1.sql
```
---

